#include <iostream>
using namespace std;

int main() {
    // Write C++ code here
    int i, j;
    cout << "Masukkan Nilai i: ";
    cin >> i;
    cout << "Masukkan Nilai j: ";
    cin >> j;
    cout << "---------------------------" << endl;
    cout << "| operasi | hasil operasi |" << endl;
    cout << "---------------------------" << endl;
    cout << "| " << i << " + " << j << "   |        " << i+j << "      |" << endl;
    cout << "| " << i << " - " << j << "   |        " << i-j << "      |" << endl;
    cout << "| " << i << " * " << j << "   |        " << i*j << "       |" << endl;
    cout << "| " << i << " div " << j << "   |        " << i/j << "     |" << endl;
    cout << "| " << i << " mod " << j << "   |        " << i%j << "  |" << endl;
cout << "---------------------------" << endl;

    return 0;
}
