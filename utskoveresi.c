#include <stdio.h>

int main() {
    float suhu_awal, suhu_akhir, step, celcius, fahrenheit, kelvin;

    // Input suhu awal, step, dan suhu akhir
    printf("Masukkan suhu awal: ");
    scanf("%f", &suhu_awal);

    printf("Masukkan step: ");
    scanf("%f", &step);

    printf("Masukkan suhu akhir: ");
    scanf("%f", &suhu_akhir);

    // Header tabel
    printf("| Celcius   | Fahrenheit   | Kelvin   |\n");

    // Konversi suhu dan tampilkan dalam bentuk tabel
    for (celcius = suhu_awal; celcius <= suhu_akhir; celcius += step) {
        fahrenheit = (celcius * 9.0 / 5.0) + 32.0;
        kelvin = celcius + 273.15;
        printf("| %-9.2f | %-12.2f | %-8.2f |\n", celcius, fahrenheit, kelvin);
    }


    return 0;
}
