#include <stdio.h>

void printStars(int n) {
    int i, j;

    // Bagian atas pola
    for (i = 1; i <= n; i++) {
        // Spasi sebelum bintang
        for (j = i; j < n; j++) {
            printf(" ");
        }
        // Bintang
        for (j = 1; j <= i; j++) {
            printf(" *");
        }
        printf("\n");
    }

    // Bagian bawah pola
    for (i = n - 1; i >= 1; i--) {
        // Spasi sebelum bintang
        for (j = n; j > i; j--) {
            printf(" ");
        }
        // Bintang
        for (j = 1; j <= i; j++) {
            printf(" *");
        }
        printf("\n");
    }
}

int main() {
    int N;

    printf("Masukkan N: ");
    scanf("%d", &N);

    if (N <= 0) {
        printf("N harus lebih besar dari 0.\n");
        return 1;
    }

    printStars(N);

    return 0;
}
