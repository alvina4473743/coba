#include <stdio.h>

int main() {
    float bilangan1, bilangan2, hasil;
    char operator;

    printf("Masukkan Bilangan ke 1: ");
    scanf("%f", &bilangan1);

    printf("Masukkan Operator (+, -, *, /): ");
    scanf(" %c", &operator);

    printf("Masukkan Bilangan ke 2: ");
    scanf("%f", &bilangan2);

    switch (operator) {
        case '+':
            hasil = bilangan1 + bilangan2;
            printf("Bilangan ke 1 + Bilangan ke 2 = %.2f + %.2f = %.2f\n", bilangan1, bilangan2, hasil);
            break;
        case '-':
            hasil = bilangan1 - bilangan2;
            printf("Bilangan ke 1 - Bilangan ke 2 = %.2f - %.2f = %.2f\n", bilangan1, bilangan2, hasil);
            break;
        case '*':
            hasil = bilangan1 * bilangan2;
            printf("Bilangan ke 1 * Bilangan ke 2 = %.2f * %.2f = %.2f\n", bilangan1, bilangan2, hasil);
            break;
        case '/':
            if (bilangan2 != 0) {
                hasil = bilangan1 / bilangan2;
                printf("Bilangan ke 1 / Bilangan ke 2 = %.2f / %.2f = %.2f\n", bilangan1, bilangan2, hasil);
            } else {
                printf("Error: Pembagian dengan nol tidak diperbolehkan.\n");
            }
            break;
        default:
            printf("Error: Operator yang dimasukkan tidak valid.\n");
    }

    return 0;
}
    
